#include "Map.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

//PRIVATE


//******************************************************************

//PUBLIC
Map::Map(Renderer::RenderContext &RC, int position, bool extra, Mover* mover) : 
	ObjectLight(Renderer::SpotLight(
		glm::vec3(0, 0, 0),
		glm::vec3(0, 1, 0),
		glm::vec3(204, 204 ,0),
		M_PI / 2.,
		1,
		0,
		500,
		0,
		RC
	)) {

	pos = position * ROADSIZE;		// Set the startposition for the piece of the road.
	hasStreetLight = extra;			// Does this particular road have an object (streetlight) attached to it
	tree = (int)mover->randNr(TREE_VARIABLES);			//How will it be a tree-setup, with greater chance of no tree for the piece of road
	treeRotation = mover->randNr(2);	//Set to a float between 0 and 2
}

void Map::createMap(Scenegraph::GroupNode &node, Renderer::Model& road, Renderer::Model& streetLight, Mover* mover) {
	glm::mat4x4 location = glm::mat4x4(1.f);

	location = glm::translate(location, glm::vec3(1, 1, pos));
	location = glm::rotate(location, PI, glm::vec3(-1, -1, 0));				//Road needs to be rotated twice
	location = glm::rotate(location, PI, glm::vec3(1, 0, 1));
	location = glm::scale(location, glm::vec3(2, 2, 2));

	node.addNode(std::make_unique<Scenegraph::GeometryNode>(road, location));

	//If the road has an extra object, set it up
	if (hasStreetLight) {

		//Add the first streetlight
		location = glm::mat4x4(1.f);
		location = glm::translate(location, glm::vec3(ROADOBJECT, 1, pos));
		location = glm::rotate(location, PI, glm::vec3(0, 1, 0));
		location = glm::scale(location, glm::vec3(50, 50, 50));
		node.addNode(std::make_unique<Scenegraph::GeometryNode>(streetLight, location));

		// Add light for the first streetlight, might be a bit off
		location = glm::mat4x4(1.f);
		location = glm::translate(location, glm::vec3(ROADOBJECT - 110, 490, pos));
		node.addNode(std::make_unique<Scenegraph::LightNode<Renderer::SpotLight>>(ObjectLight, location));

		//Add the second streetlight
		location = glm::mat4x4(1.f);
		location = glm::translate(location, glm::vec3(-ROADOBJECT, 1, pos));
		location = glm::scale(location, glm::vec3(50, 50, 50));
		node.addNode(std::make_unique<Scenegraph::GeometryNode>(streetLight, location));

		//Add light for the second streetlight
		location = glm::mat4x4(1.f);
		location = glm::translate(location, glm::vec3(-ROADOBJECT + 110, 490, pos));
		node.addNode(std::make_unique<Scenegraph::LightNode<Renderer::SpotLight>>(ObjectLight, location));
	}

	float oldPos = pos;

	pos = mover->movement(pos);		//Update position

	//New tree when the road resets
	if (oldPos > pos + ROADSIZE * 3)
		tree = (int)mover->randNr(TREE_VARIABLES);
}

void Map::renderTree(Scenegraph::GroupNode &node, Renderer::Model& treeObject) {
	glm::mat4x4 location = glm::mat4x4(1.f);
	
	//More possible numbers can be added for more positions for each tree
	//Can also make numbers for a greater number of trees
	//std::cout << "Tree is: " << tree << std::endl;
	switch (tree) {
	case 0:
		return;	//There is no tree
	case 1:
		//Left side of the road
		location = glm::translate(location, glm::vec3(-2.5 * ROADOBJECT, -10, pos));
		location = glm::rotate(location, PI, glm::vec3(0, treeRotation, 0));
		location = glm::scale(location, glm::vec3(50, 50, 50));
        location = glm::translate(location, glm::vec3(0, 0, 471));
		node.addNode(std::make_unique<Scenegraph::GeometryNode>(treeObject, location));
		break;
	case 2:
		//Right side of the road
		location = glm::translate(location, glm::vec3(2.5 * ROADOBJECT, -10, pos));
		location = glm::rotate(location, PI, glm::vec3(0, treeRotation, 0));
		location = glm::scale(location, glm::vec3(50, 50, 50));
		location = glm::translate(location, glm::vec3(0, 0, 471));
		node.addNode(std::make_unique<Scenegraph::GeometryNode>(treeObject, location));
		break;
	default:
		return;	//Not a legit number, no tree
	}
}
