#include "movement.h"
#include "consts.h"
#include <math.h>

//DEBUG
#include <iostream>

Mover::Mover() {
	ms = std::chrono::duration_cast<std::chrono::milliseconds>(
		std::chrono::system_clock::now().time_since_epoch()
		);
	time = ms.count();

	timeStart = time;
	previousTime = time;
	previousTimeRelPos = time;
	previousTimeNoteSpawn = time;

	srand(time);

	speedIncrease = 10.0f;
	pointMult = 100.0f;		//Initial point increase

	notesCollected = 0;		//No notes will be collected at the very start
	combo = 0;				//Comboes always start at 0
	carsAvoided = 0;		//Avoided cars always start at 0;
}

float Mover::movement(float currentPos) {
	float pos = currentPos;
	pos -= ROADSPEED * (time - previousTime) * powf(log2f(speedIncrease), 0.75f) * 0.6f;		//Changes the position of the road
	if (pos <= -(ROADSIZE * 3.5))		//If the road is behind the car, then the road will be relocated to the front
		pos += MAXROAD * ROADSIZE;

	return pos;
}

bool Mover::noteSpawn() {
	if (previousTimeNoteSpawn + MAX_SPAWN_DELAY - powf(speedIncrease, 1.75f) > time)
		return false;
	previousTimeNoteSpawn = time;
	return true;
}

int Mover::getPoints() {
	notesCollected++;
	combo++;

	if (combo % 50 == 0)
		pointMult *= 1.2f;		//Increase the multiplier for every 50 notes collected in a combo

	if (notesCollected % 100 == 0)
		pointMult *= 1.05f;		//Less increase in multiplier every time the player has collected 100 more notes

	return pointMult;			//Return whatever it has become. Note, will be become greater after some time
}

void Mover::comboBreak() {
	int reduce = combo / 50;		//Get the amount of times the combo breached 50x

	pointMult /= powf(50, reduce);		//Remove the multiplier from combo
}

void Mover::carDespawn() {
	carsAvoided++;

	if (carsAvoided % 150 == 0)
		pointMult *= log2f(time - timeStart) * 0.6f;		//The more time has passed, the more points
}

void Mover::increaseSpeed(int points) {
	speedIncrease = 10.0f + log2f(points + 1);						//Current speedincrease
}

void Mover::resetValues() {
	notesCollected = 0;
	combo = 0;
	pointMult = 10.f;
	carsAvoided = 0;
}

void Mover::updateTime() {
	previousTime = time;
	ms = std::chrono::duration_cast<std::chrono::milliseconds>(
		std::chrono::system_clock::now().time_since_epoch()
		);
	time = ms.count();
}

float Mover::randNr(int limit) {

	return rand() % limit;
}