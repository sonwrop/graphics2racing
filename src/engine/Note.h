#pragma once

#include "../renderer/RenderContext.h"
#include "../renderer/Model.h"
#include "../renderer/scenegraph/GroupNode.h"
#include "../renderer/scenegraph/GeometryNode.h"
#include "../renderer/scenegraph/PerspectiveCameraNode.h"
#include "../renderer/scenegraph/LightNode.h"
#include "../renderer/SpotLight.h"
#include "consts.h"
#include "movement.h"

#include <glm/ext/matrix_transform.hpp>

// Class for notes objects
class Note {
private:
	Renderer::SpotLight noteLight;																	// Light of note
	int x;																						// relative x coordinates
	float y;																						// y coordinates
	glm::vec3 color;
public:
	Note(Renderer::RenderContext &, glm::vec3, int);				// constructs the note with context, color, model, speed and road location
	void createNote(Scenegraph::GroupNode &, Renderer::Model&);										// FUnction which adds note to the provided scenegraph
	void renderAndMove(Scenegraph::GroupNode &, Renderer::Model&, Mover* mover);						// calls createNode, in addition to modify x, y position
	bool isBehind();																				// Checks if the note is behind the car
	bool isUnderCar(int);																			// Checks if current note is below the car
};