#pragma once

#include "movement.h"
#include "obstacle.h"
#include "Note.h"
#include <string>
#include "../renderer/Model.h"
#include "../renderer/scenegraph/GroupNode.h"


float translateRelativePos(int); // Translates field 0-4 to actual position
int NoteControl(Scenegraph::GroupNode &node, Renderer::Model& noteObject, Mover* mover, int carPos, std::vector<Note>& notes);
bool ObstacleControl(Scenegraph::GroupNode &node, Renderer::Model& car, Mover* mover, int carPos, std::vector<Obstacle>& obstacles);
void patternControl(Renderer::RenderContext &renderer, Mover* mover, std::vector<Obstacle>& obstacles, std::vector<Note>& notes, int& patternCount, int pattern[MAX_PATTERNS][5]);
