#include "Functions.h"
#include "consts.h"
#include <fstream>

// Translates relative position(0-4) to actual position on the road
float translateRelativePos(int field) {
	switch (field) {
	case 0:
		return -2 * OFFSET;
		break;
	case 1:
		return -OFFSET;
		break;
	case 3:
		return OFFSET;
		break;
	case 4:
		return 2 * OFFSET;
		break;
	default: // 2 and all invalid vlaues return 0(middle)
		return 0;
		break;
	}
}


int NoteControl(Scenegraph::GroupNode &node, Renderer::Model& noteObject, Mover* mover, int carPos, std::vector<Note>& notes) {
	int points = 0;
	// Goes through all notes and checks if they should be deleted, and adds to the score if the car is above them
	for (int i = 0; i < notes.size(); i++) {
		//Notes
		if (notes[i].isBehind()) { // if the note is behind the car and should be despawned
			notes.erase(notes.begin() + i);
			i--;
			mover->comboBreak();		//Note missed, combo is broken
		}
		else if (notes[i].isUnderCar(carPos)) { // if the car is over the note, currently does the same as ordinary despawn but also adds point a point
			notes.erase(notes.begin() + i);
			i--;
			points = mover->getPoints();	//Get the points
		}
		else {
			notes[i].renderAndMove(node, noteObject, mover); // Renders the node and updates its y coordinates
		}
	}

	return points;
}

bool ObstacleControl(Scenegraph::GroupNode &node, Renderer::Model& car, Mover* mover, int carPos, std::vector<Obstacle>& obstacles) {
	for (int i = 0; i < obstacles.size(); i++) {
		//Obstacles
		if (obstacles[i].behindMap()) {
			obstacles.erase(obstacles.begin() + i);
			i--;
			mover->carDespawn();		//Update that another car is despawned for points
		}
		else if (obstacles[i].carCrash(carPos)) {
			obstacles.erase(obstacles.begin() + i);
			i--;

			//End game? Add that stuff here
			return false;
		}
		else {
			//Send the Model of the right car to the rendering
			switch (obstacles[i].carNum()) {
			case 0:
				obstacles[i].render(node, car, mover);
				break;
			default:
				//Should not be possible, no rendering happends

				//DEBUG
				obstacles[i].render(node, car, mover);

				break;
			}
		}
	}

	return true;
}

void patternControl(Renderer::RenderContext &renderer, Mover* mover, std::vector<Obstacle>& obstacles, std::vector<Note>& notes, int& patternCount, int pattern[MAX_PATTERNS][5]) {

	for (int i = 0; i < 5; i++) {
		switch (pattern[patternCount][i]) {
		case 0: break;
		case 1:
			notes.push_back(Note(renderer, COLORS[rand() % COLORS->length()], i));
			break;
		case 2:
			obstacles.push_back(Obstacle(i, (int)mover->randNr(1)));
			break;
		default: break;
		}
	}

	patternCount++;
}