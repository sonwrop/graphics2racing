#pragma once

#include "../renderer/Model.h"
#include "../renderer/scenegraph/GroupNode.h"
#include "../renderer/scenegraph/GeometryNode.h"
#include "movement.h"

class Obstacle {
private:
	int car;
	int posX;
	float posY;

public:
	Obstacle(int field, int carObstacle);
	void render(Scenegraph::GroupNode &node, Renderer::Model& car, Mover* mover);
	int carNum();
	bool behindMap();
	bool carCrash(int carPos);
};