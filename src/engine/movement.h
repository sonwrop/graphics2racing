#pragma once

#include <chrono>

class Mover {
private:
	std::chrono::milliseconds ms;
	float speedIncrease;
	float pointMult;
	int notesCollected;
	int combo;
	int carsAvoided;
	long int time;
	long int timeStart;
	long int previousTime;
	long int previousTimeRelPos;
	long int previousTimeNoteSpawn;
public:
	Mover();
	float movement(float currentPos);
	bool noteSpawn();
	int getPoints();
	void comboBreak();
	void carDespawn();
	void increaseSpeed(int points);
	void resetValues();
	void updateTime();
	float randNr(int limit);
};