#pragma once

#include "../renderer/RenderContext.h"
#include "../renderer/Model.h"
#include "../renderer/scenegraph/GroupNode.h"
#include "../renderer/scenegraph/GeometryNode.h"
#include "../renderer/scenegraph/PerspectiveCameraNode.h"
#include "../renderer/SpotLight.h"
#include "../renderer/scenegraph/LightNode.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/ext/matrix_transform.hpp>
#include <iostream>

#include "consts.h"
#include "movement.h"

class Map {
private:
	float pos;				// Position for each road
	bool hasStreetLight;	// Does the particular map have an extra object attached to it
	int tree;				// Is there a tree for this part of the road (0 if not)
							// and the side for it (1 is left, 2 is right)
	float treeRotation;		// How much is the tree rotated
	Renderer::SpotLight ObjectLight;

public:
	Map(Renderer::RenderContext &RC, int position, bool extra, Mover* mover);
	void createMap(Scenegraph::GroupNode &node, Renderer::Model& road, Renderer::Model& streetLight, Mover* mover);
	void renderTree(Scenegraph::GroupNode &node, Renderer::Model& road);
};