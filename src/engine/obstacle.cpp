#include "obstacle.h"
#include "consts.h"
#include "Functions.h"

Obstacle::Obstacle(int field, int carObstacle) {
	posX = field;
	posY = ROADSIZE * MAXROAD / 1.8;

	car = carObstacle;
}

void Obstacle::render(Scenegraph::GroupNode &node, Renderer::Model& car, Mover* mover) {
	glm::mat4x4 location = glm::mat4x4(1.f);

	location = glm::translate(location, glm::vec3(translateRelativePos(posX), 6, posY));
	location = glm::rotate(location, PI, glm::vec3(0, 1, 0));

	node.addNode(std::make_unique<Scenegraph::GeometryNode>(car, location));

	posY = mover->movement(posY);
}

int Obstacle::carNum() {
	return car;
}

bool Obstacle::behindMap() {
	return (posY < -3 * ROADSIZE);
}

bool Obstacle::carCrash(int carPos) {
	return (posY < UNDER_CAR_DISTANCE && posX == carPos && posY > -UNDER_CAR_DISTANCE);
}