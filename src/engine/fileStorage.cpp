#include "fileStorage.h"

#include <iostream>


// Reads local data(if it extsts) and updates an int
void updateHighScore(int highScore) {
	std::ifstream in(FILE_NAME);

	int table[10];

	if (in) {
		int i = 0;
		while (!in.eof() && i < 10) {
			in >> table[i++];
			//Because of some weird reason, I need to double ignore
			in.ignore();
			in.ignore();
		}

		//No need to keep it open any longer
		in.close(); // closes fstream

		if (i < 10) {
			table[i++] = highScore;
			storeData(table, i);
			return;	//It has been updated, no need to continue with the code
		}

		if (table[9] > highScore) {
			return;	//Not a new top 10 high score, no update
		}

		table[9] = highScore; //Dump lowest score
		for (i; i >= 0; --i) {
			//Sort the table
			if (table[i] > table[i - 1]) {
				int k = table[i - 1];
				table[i - 1] = table[i];
				table[i] = k;
			}
			else {
				//The new high score has found it's place in the table
				storeData(table, 10);

				return;	//Updated, no need to continue the loop or anything else 
			}
		}
	}

	else {
		//No data stored from before.
		table[0] = highScore;
		storeData(table, 1);
	}

	//Close it just so it does not stay open
	in.close(); // closes fstream

}

// Stores data onto local file
void storeData(int highScore[], int count) {
	std::ofstream out(FILE_NAME);

	for (int i = 0; i < count; i++) {
		out << highScore[i] << std::endl;
	}

	out.close(); // closes fstream
}