#include "Car.h"

#define _USE_MATH_DEFINES
#include <math.h>

Car::Car(Renderer::RenderContext &RC, int relativePosition) :
	headLight(Renderer::SpotLight(
		glm::vec3(0, 70, 180),
		glm::normalize(glm::vec3(0, 0.1, -1)),
		glm::vec3(500, 500, 500),
		M_PI / 5.,
		1.,
		0.,
		500,
		0,
		RC
	)) {
	relativePos = relativePosition;

	previousMovementTime = getCurrentTime();
}

void Car::createCar(Renderer::Model& testModel, Scenegraph::GroupNode &node, int relativePos, int previousRelativePos, int currentPosProgress) {
	glm::mat4x4 carLocation = glm::mat4x4(1.f);

	// Position calculation	
	// int pos = (translateRelativePos(previousRelativePos) * (CAR_ANIMATION_STEPS - currentPosProgress) + translateRelativePos(relativePos) * currentPosProgress) / CAR_ANIMATION_STEPS;
	// Uses sin to get animation to be quicker in the middle and end
	float sinValue = sin(-PI / 2 + PI * ((float)currentPosProgress / CAR_ANIMATION_STEPS));	// Component of position calculation
	int pos = (translateRelativePos(previousRelativePos) * (1 - (sinValue + 1) / 2) + translateRelativePos(relativePos) * (sinValue + 1) / 2);

	carLocation = glm::translate(carLocation, glm::vec3(pos, 1, 1));

	node.addNode(std::make_unique<Scenegraph::GeometryNode>(testModel, carLocation));
	node.addNode(std::make_unique<Scenegraph::LightNode<Renderer::SpotLight>>(
		headLight,
		glm::translate(glm::mat4(1), glm::vec3(pos+80, 0, 0))
	));
	node.addNode(std::make_unique<Scenegraph::LightNode<Renderer::SpotLight>>(
		headLight,
		glm::translate(glm::mat4(1), glm::vec3(pos-80, 0, 0))
	));
}

// Returns current time
long int Car::getCurrentTime() {
	ms = std::chrono::duration_cast<std::chrono::milliseconds>(
		std::chrono::system_clock::now().time_since_epoch()
		); 
	return ms.count();
}