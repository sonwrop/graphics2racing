#pragma once

#include "../renderer/RenderContext.h"
#include "../renderer/Model.h"
#include "../renderer/scenegraph/GroupNode.h"
#include "../renderer/scenegraph/GeometryNode.h"
#include "../renderer/scenegraph/PerspectiveCameraNode.h"
#include "../renderer/SpotLight.h"
#include "../renderer/scenegraph/LightNode.h"
#include "Functions.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/ext/matrix_transform.hpp>
#include <iostream>

#include "consts.h"
#include "movement.h"

class Car {
private: 
	std::chrono::milliseconds ms;
	int relativePos;
	long int previousMovementTime;
	Renderer::SpotLight headLight;
public:
	Car(Renderer::RenderContext &RC, int relativePosition);
	void createCar(Renderer::Model& testModel, Scenegraph::GroupNode &node, int relativePosition, int previousRelativePos, int currentPosProgress);
	long int getPreviousTimeModified() { return previousMovementTime; }
	long int getCurrentTime();
	void updatePreviousTime(long int offset = 0) { previousMovementTime = getCurrentTime() - offset; }
};