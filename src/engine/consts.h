#pragma once

#include <glm/ext/matrix_transform.hpp>
#include <string>

const float DISTANCE_CAMERA = 1500;		// Distance of the camera to car
const float MAX_SPAWN_DELAY = 800;		// The maximum spawn delay of notes
const float NOTE_HEIGHT = 50;			// Height above xz plane the note spawns
const float NOTE_SCALE = 25;			// Scale of note
const float PI = 3.14159f;				// Approximate value of PI
const float ROADSPEED = 2.5f;			// Speed the road moves at

const int BEHIND_CAR = -200;			// Location at which notes and such despawn behind the car
const int CAR_ANIMATION_STEPS = 40;		// Number of animation steps for player controlled car
const int CAR_ANIMATION_TIME = 2;		// Milliseconds per animation step
const int MAX_PATTERNS = 30;			// Amount of rows a pattern can have
const int MAXROAD = 60;					// Maximum number of roads
const int OFFSET = 266;					// The offset in distance between notes and such to hold them in the middle of their felds on the road
const int PATTERNCOUNT = 6;					//Amount of patterns in the array
const int ROADSIZE = 600;				// Size of roads
const int ROADOBJECT = 650;				// How far from the center of the road it's objects are
const int TREE_VARIABLES = 6;			// Amount of different tree-patterns the game can have
const int UNDER_CAR_DISTANCE = 300;		// Distance from 0 to register collection of item

const char FILE_NAME[] = "highScore.data";	// Name of file data such as highScore is stored

// Array of colors used for notes
const glm::vec3 COLORS[] = {
	glm::vec3(200, 0, 0),
	glm::vec3(0, 200, 0),
	glm::vec3(0, 0, 200),
	glm::vec3(100, 100, 0),
	glm::vec3(0, 100, 100),
	glm::vec3(100, 0, 100),
	glm::vec3(150, 50, 50),
	glm::vec3(50, 150, 50),
	glm::vec3(50, 50, 150),
	glm::vec3(100, 0, 0),
	glm::vec3(0, 100, 0),
	glm::vec3(0, 0, 100),
	glm::vec3(225, 51, 0) };
