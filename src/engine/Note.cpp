#include "Note.h"
#include "Functions.h"

#define _USE_MATH_DEFINES
#include <math.h>

// Generates note and translates field value into the correct x and y coordinates
Note::Note(Renderer::RenderContext &renderer, glm::vec3 color, int field) :
    noteLight(Renderer::SpotLight(
        glm::vec3(0, 10, 0),
        glm::vec3(0, 1, 0),
        color * 10.f,
        M_PI / 2.,
        1,
        0,
        400,
        0,
        renderer
    )) {
	x = field;

	y = ROADSIZE * MAXROAD / 1.8;	// y value(flat plane)

	this->color = color;
}

// Adds note to scenegraph
void Note::createNote(Scenegraph::GroupNode &node, Renderer::Model &noteModel) {
	glm::mat4x4 location = glm::mat4x4(1.f);

    location = glm::translate(location, glm::vec3(translateRelativePos(x), NOTE_HEIGHT, y));
    location = glm::rotate(location, PI, glm::vec3(0, 1, 0));
	location = glm::scale(location, glm::vec3(NOTE_SCALE, NOTE_SCALE, NOTE_SCALE));

	Renderer::ExtraMaterial extraMaterial{};
	extraMaterial.emissive = color / 200.f;
	node.addNode(std::make_unique<Scenegraph::GeometryNode>(noteModel, location, extraMaterial));

    node.addNode(std::make_unique<Scenegraph::LightNode<Renderer::SpotLight>>(noteLight, location));
}

// Renders and updates coordinates based on speed
void Note::renderAndMove(Scenegraph::GroupNode& node, Renderer::Model& model, Mover* mover) {
	createNote(node, model); // renders node
	
	y = mover->movement(y);
}

// Checks if the note is behind the car
bool Note::isBehind() {
	return y < BEHIND_CAR;
}

// 0..4
// Returns if the note is under the car
bool Note::isUnderCar(int relativeCarPos) {
	return (y < UNDER_CAR_DISTANCE && x == relativeCarPos);
}
