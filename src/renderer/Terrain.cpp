#include <glm/ext/matrix_transform.hpp>
#include "Terrain.h"

#include "RenderContext.h"

using namespace Renderer;

template<class T>
class PushBuffer {
public:
    explicit PushBuffer(MappedBuffer<T> &mappedBuffer) : m_mappedBuffer(&mappedBuffer), m_index(0) {}

    size_t size() const {
        return m_index;
    }

    void push(T value) {
        assert((GLsizei) m_index < m_mappedBuffer->length());
        m_mappedBuffer->items()[m_index] = std::move(value);
        m_index++;
    }

private:
    MappedBuffer<T> *m_mappedBuffer;
    size_t m_index;
};

void pushRectLine(glm::vec2 startPos, glm::vec2 endPos, PushBuffer<Terrain::LineVertex> &vertices, PushBuffer<uint32_t> &indices) {
    size_t startIndex = vertices.size();
    vertices.push({
        glm::vec2(0, 0),
        startPos,
        endPos
    });
    vertices.push({
        glm::vec2(1, 0),
        startPos,
        endPos
    });
    vertices.push({
        glm::vec2(0, 1),
        startPos,
        endPos
    });
    vertices.push({
        glm::vec2(1, 1),
        startPos,
        endPos
    });

    indices.push(startIndex + 0);
    indices.push(startIndex + 1);
    indices.push(startIndex + 2);

    indices.push(startIndex + 0);
    indices.push(startIndex + 2);
    indices.push(startIndex + 3);
}

Terrain::Terrain(Renderer::RenderContext &renderContext)
    : m_renderContext(&renderContext),
      m_lineVertices(4*2, GL_ARRAY_BUFFER, GL_STATIC_DRAW),
      m_lineIndices(6*2, GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW),
      m_quadVertices(4, GL_ARRAY_BUFFER, GL_STATIC_DRAW),
      m_quadIndices(6, GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW),
      m_worldOffset(0) {
    {
        auto vertexBuffer = m_quadVertices.map();
        auto indexBuffer = m_quadIndices.map();
        PushBuffer<glm::vec2> vertices(vertexBuffer);
        PushBuffer<uint32_t> indices(indexBuffer);

        vertices.push(glm::vec2(0, 0));
        vertices.push(glm::vec2(1, 0));
        vertices.push(glm::vec2(1, 1));
        vertices.push(glm::vec2(0, 1));

        indices.push(2);
        indices.push(1);
        indices.push(0);

        indices.push(3);
        indices.push(2);
        indices.push(0);
    }
    {
        auto vertexBuffer = m_lineVertices.map();
        auto indexBuffer = m_lineIndices.map();
        PushBuffer<LineVertex> vertices(vertexBuffer);
        PushBuffer<uint32_t> indices(indexBuffer);

        pushRectLine(glm::vec2(0, 0), glm::vec2(1, 0), vertices, indices);
        pushRectLine(glm::vec2(1, 0), glm::vec2(1, 1), vertices, indices);
    }

    m_lineVao.bind();
    m_lineVertices.bind();
    m_lineIndices.bind();
    m_lineVao.addAttrib(2, 6, 0); // line pos
    m_lineVao.addAttrib(2, 6, 2); // rect start pos
    m_lineVao.addAttrib(2, 6, 4); // rect end pos
    m_lineVao.unbind();

    m_quadVao.bind();
    m_quadVertices.bind();
    m_quadIndices.bind();
    m_quadVao.addAttrib(2, 2, 0);
    m_quadVao.unbind();
}

void Terrain::setWorldOffset(float offset) {
    m_worldOffset = offset;
}

const glm::ivec2 MAP_SIZE = glm::ivec2(100, 200);
const glm::vec2 TILE_SIZE = glm::vec2(200, 200);
const float LINE_THICKNESS = 0.0015;

void Terrain::render(glm::mat4x4 transform) const {
    auto offsetTransform = transform * glm::translate(
        glm::mat4x4(1.f),
        glm::vec3(0, 0, ::fmodf(m_worldOffset, TILE_SIZE.y))
    );

    // Render the terrain quads
    auto &quadProgram = m_renderContext->terrainProgram();
    quadProgram.setUniform("mapSize", MAP_SIZE);
    quadProgram.setUniform("tileSize", TILE_SIZE);
    quadProgram.setUniform("worldOffset", glm::vec2(0, -m_worldOffset));
    quadProgram.setUniform("modelMatrix", offsetTransform);

    m_quadVao.bind();
    m_quadVertices.bind();
    m_quadIndices.bind();
    quadProgram.bind();
    glDrawElementsInstanced(GL_TRIANGLES, m_quadIndices.length(), GL_UNSIGNED_INT, nullptr, MAP_SIZE.x * MAP_SIZE.y);
    m_quadVao.unbind();

    // Render the outlines
    auto &lineProgram = m_renderContext->terrainOutlineProgram();
    lineProgram.setUniform("mapSize", MAP_SIZE);
    lineProgram.setUniform("tileSize", TILE_SIZE);
    lineProgram.setUniform("lineThickness", LINE_THICKNESS);
    lineProgram.setUniform("worldOffset", glm::vec2(0, -m_worldOffset));
    lineProgram.setUniform("modelMatrix", offsetTransform);

    m_lineVao.bind();
    m_lineVertices.bind();
    m_lineIndices.bind();
    lineProgram.bind();
    glDrawElementsInstanced(GL_TRIANGLES, m_lineIndices.length(), GL_UNSIGNED_INT, nullptr, MAP_SIZE.x * MAP_SIZE.y);
    m_lineVao.unbind();

    // Re-bind the standard mesh program so later render passes use it
    m_renderContext->geomProgram().bind();
}
