#pragma once

#include <glm/mat4x4.hpp>

#include "VertexAttribArray.h"
#include "Buffer.h"

namespace Renderer {

    class RenderContext;

    class Terrain {
    public:
        struct LineVertex {
            glm::vec2 linePos;
            glm::vec2 rectStartPos;
            glm::vec2 rectEndPos;
        };

        explicit Terrain(RenderContext &renderContext);

        float worldOffset() const { return m_worldOffset; }

        void setWorldOffset(float offset);

        void render(glm::mat4x4 transform) const;

    private:
        RenderContext *m_renderContext;
        VertexAttribArray m_lineVao;
        Buffer<LineVertex> m_lineVertices;
        Buffer<uint32_t> m_lineIndices;
        VertexAttribArray m_quadVao;
        Buffer<glm::vec2> m_quadVertices;
        Buffer<uint32_t> m_quadIndices;
        float m_worldOffset;
    };

}
