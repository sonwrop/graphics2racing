#pragma once

#include "ImageTexture.h"

struct aiMaterial;

namespace Renderer {

    class ShaderProgram;

    class Material {
    public:
        explicit Material(const aiMaterial *mat, const char *modelDirectory);

        void bind() const;

    private:
        ImageTexture m_albedo;
        ImageTexture m_metallic;
        ImageTexture m_roughness;
        ImageTexture m_emissive;
    };

}
