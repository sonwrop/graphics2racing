#define GFX_NO_TERMINATION_ON_GL_ERROR
#define GFX_IMPLEMENTATION
#include <GFX/gfx.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/ext/matrix_transform.hpp>

#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"

#include <iostream>
#include <fstream>

#include <Windows.h>
#pragma comment(lib, "winmm.lib")

#define _USE_MATH_DEFINES
#include <math.h>
#include <chrono>

#include "engine/consts.h"
#include "engine/map.h"
#include "engine/movement.h"
#include "engine/Car.h"
#include "engine/Note.h"
#include "engine/obstacle.h"
#include "engine/Functions.h"

#include "renderer/RenderContext.h"
#include "renderer/Model.h"
#include "renderer/DirectionalLight.h"
#include "renderer/ShadowDirectionalLight.h"
#include "renderer/scenegraph/GroupNode.h"
#include "renderer/scenegraph/GeometryNode.h"
#include "renderer/scenegraph/PerspectiveCameraNode.h"
#include "renderer/scenegraph/LightNode.h"
#include "renderer/scenegraph/CustomNode.h"

#include "engine/consts.h"
#include "engine/fileStorage.h"
#include "engine/map.h"
#include "engine/movement.h"
#include "engine/Car.h"
#include "engine/Note.h"
#include "engine/obstacle.h"
#include "engine/Functions.h"
#include "renderer/SpotLight.h"
#include "renderer/AmbientLight.h"
#include "renderer/Terrain.h"

int main() {
	if (!glfwInit()) {
		GFX_ERROR("Failed to initialize GLFW");
	}

	const char* glsl_version = "#version 330";
	bool show_score = true;
	bool start_game = true;
	bool exit_game = false;

	glfwWindowHint(GLFW_SAMPLES, 1);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	auto window = glfwCreateWindow(1280, 720, "ARFRD", nullptr, nullptr);
	if (!window) {
		glfwTerminate();
		GFX_ERROR("Failed to create a GLFW window");
	}

	glfwMakeContextCurrent(window);
	if (glewInit() != GLEW_OK) {
		glfwTerminate();
		GFX_ERROR("Failed to initialize GLEW");
	}

	GFX_GL_CALL(glClearColor(0, 0, 0, 1));
	GFX_GL_CALL(glEnable(GL_CULL_FACE));
	GFX_GL_CALL(glFrontFace(GL_CW));
	GFX_GL_CALL(glClipControl(GL_LOWER_LEFT, GL_ZERO_TO_ONE));
	GFX_GL_CALL(glDepthFunc(GL_GREATER));

	// Context of rendering
	Renderer::RenderContext renderContext;

	PlaySound("resources/Sound/Music.wav", NULL, SND_LOOP | SND_ASYNC);

	// Loading models
	Renderer::Model testModel = Renderer::Model::fromObjFile("aventador/Lamborghini_Aventador.obj", renderContext);
	Renderer::Model noteObject = Renderer::Model::fromObjFile("Notes/21451_Musical_Note_V2.obj", renderContext);
	Renderer::Model roadObject = Renderer::Model::fromObjFile("RoadSectionStraight/Road.obj", renderContext);
	Renderer::Model streetLightObject = Renderer::Model::fromObjFile("StreetLight/StreetLight.obj", renderContext);
	Renderer::Model tree = Renderer::Model::fromObjFile("palm/Date Palm.obj", renderContext);

	// Menu texture start
	unsigned int startTexture;
	glGenTextures(1, &startTexture);
	glBindTexture(GL_TEXTURE_2D, startTexture);
	// load and generate the texture
	int width, height, nrChannels;
	unsigned char *textureData = stbi_load("resources/Textures/buttonStartNormal.png", &width, &height, &nrChannels, STBI_rgb_alpha);
	if (textureData)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(textureData);

	unsigned int exitTexture;
	glGenTextures(1, &exitTexture);
	glBindTexture(GL_TEXTURE_2D, exitTexture);
	// load and generate the texture
	int nrChannelsExit;
	textureData = stbi_load("resources/Textures/buttonExitNormal.png", &width, &height, &nrChannelsExit, STBI_rgb_alpha);
	if (textureData)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(textureData);

	// OLD
	float previousMousePosX, previousMousePosY; // Previous mosePos

	// Movement object
	Mover* mover;
	mover = new Mover();

	int patterns[MAX_PATTERNS][5];

	Map* road[MAXROAD];

	// Spawns roads
	for (int i = 0; i < MAXROAD; i++) {
		bool extra = false;
		if (i % 10 == 1)
			extra = true;
		road[i] = new Map(renderContext, i, extra, mover);
	}

	// Note* notes[50];
	std::vector<Note> notes;
	std::vector<Obstacle> obstacles;

	// Terrain
	Renderer::Terrain terrain(renderContext);
	Renderer::ShadowDirectionalLight sunLight(
		Renderer::DirectionalLight(
			glm::normalize(glm::vec3(0, 0.5, 1)),
			glm::vec3(2, 2, 0.1),
			0,
			renderContext
		),
		glm::vec3(0, 0, 4000),
		glm::vec3(0, 1, 0),

		glm::vec2(-780, 780),
		glm::vec2(0, 4000),
		glm::vec2(0, 11000),

		glm::uvec2(4096, 4096),
		renderContext
	);

	// Car location variables
	int carRelativeLocation = 2;			// Relative position of car, value is 0-4
	int currentRelativeLocationValue = 0;	// Exact movmeent value
	int previousRelativeLocation = 2;		// Previous relative position of car, value 0-4
	int patternCount = 0;

	int points = 0;	// Number of points gathered

	Car* car = new Car(renderContext, carRelativeLocation);

	// TimePassedValue, value used to increase road and note speed, WIP, currently does nothing
	float timePassedValue = 0;

	bool carNotMoving = true; // checks if the car is allowed to move at the moment

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui::StyleColorsDark();
	ImGui_ImplOpenGL3_Init(glsl_version);

	// Game loop
	while (!glfwWindowShouldClose(window) && glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !exit_game) {
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		ImGuiWindowFlags window_flags = 0;
		window_flags |= ImGuiWindowFlags_NoTitleBar;
		window_flags |= ImGuiWindowFlags_NoCollapse;
		window_flags |= ImGuiWindowFlags_NoScrollbar;
		window_flags |= ImGuiWindowFlags_NoResize;
		window_flags |= ImGuiWindowFlags_NoBackground;
		ImGui::SetNextWindowPos(ImVec2(20, 20));
		ImGui::SetNextWindowSize(ImVec2(450, 60));
		{
			ImGui::Begin("Score", &show_score, window_flags);
			ImGui::SetWindowFontScale(3.0f);
			ImGui::TextColored(ImVec4(0.0f, 0.0f, 0.0f, 1.0f), "Score: %d", points);
			ImGui::End();
		}

		if (start_game) {
			ImGui::SetNextWindowPos(ImVec2(435, 100));
			ImGui::SetNextWindowSize(ImVec2(410, 520));
			{
				ImGui::Begin("Menu", &start_game, window_flags);

				ImGui::ImageButton((void*)(intptr_t)startTexture, ImVec2(width, height));
				if (ImGui::IsItemActivated())
					start_game = false;

				ImGui::ImageButton((void*)(intptr_t)exitTexture, ImVec2(width, height));
				if (ImGui::IsItemActivated())
					exit_game = true;

				ImGui::End();
			}
		}

		if (!start_game) {
			int windowWidth, windowHeight;
			glfwGetFramebufferSize(window, &windowWidth, &windowHeight);

			// MousePos
			double xpos, ypos;
			glfwGetCursorPos(window, &xpos, &ypos);
			previousMousePosX = xpos, previousMousePosY = ypos; // Updates values

			// variable with cameraPos to be used everywhere it is needed to uphold consistency without annoyances
			glm::vec3 cameraPos = glm::vec3(0, 200., -500.);
			glm::vec3 carPos = glm::vec3(0, 0, 0);

			// Angle towards car
			glm::vec4 angleToCar = glm::normalize(glm::vec4(cameraPos, 0) - glm::vec4(carPos, 0)/*Position of car*/) * glm::rotate(
				glm::mat4x4(1.f),																	// Identify matrix
				0.0f,																				// Angle to rotate
				//(float)((ypos - previousMousePosX) / 200.0f),										// Comment out the above and uncomment this to rotate camera around car again
				glm::vec3(0, 1, 0));																// Defiens the up direction

			// Detects a and d key-presses and checks if car is moving
			if (carRelativeLocation == previousRelativeLocation) { // The car is not moving
				currentRelativeLocationValue = 0; // Resets value

				if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS && carRelativeLocation < 4) {   // D key, move right
					carRelativeLocation++;
					car->updatePreviousTime();
				}
				else if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS && carRelativeLocation > 0) {  // A key, move left
					carRelativeLocation--;
					car->updatePreviousTime(); // updates time
				}
			}
			else if (currentRelativeLocationValue < CAR_ANIMATION_STEPS) { // If the car is moving to the side
				if (car->getPreviousTimeModified() + CAR_ANIMATION_TIME <= car->getCurrentTime()) {
					currentRelativeLocationValue += (car->getCurrentTime() - car->getPreviousTimeModified()) / CAR_ANIMATION_TIME;	// Adds correct number to the animation steps

					// Checks if the naimation is finished and forces it to then not move further then it should be able to
					if (currentRelativeLocationValue > CAR_ANIMATION_STEPS)
						currentRelativeLocationValue = CAR_ANIMATION_STEPS;

					car->updatePreviousTime((car->getCurrentTime() - car->getPreviousTimeModified()) % CAR_ANIMATION_TIME);			// Updates current time
				}
			}
			else // The movement is finished
				previousRelativeLocation = carRelativeLocation; // Animation finished

			// Car node
			Scenegraph::GroupNode node(glm::translate(glm::mat4x4(1.f), carPos));

			car->createCar(testModel, node, carRelativeLocation, previousRelativeLocation, currentRelativeLocationValue);	// creates car

			node.addNode(std::make_unique<Scenegraph::PerspectiveCameraNode>(						// Camera
				glm::vec3(angleToCar * DISTANCE_CAMERA) + glm::vec3(0, 0, 500),						// pos
				angleToCar,																			// forward
				glm::vec3(0., 1., 0.),                     											// up
				45.f * M_PI / 180.f,                                      							// fov
				(float)windowWidth / (float)windowHeight, 										// aspect
				0.01f,                                     											// near z
				20000.f                                     										// far z
				));

			node.addNode(std::make_unique<Scenegraph::LightNode<Renderer::ShadowDirectionalLight>>(
				sunLight,
				glm::mat4x4(1.f)
				));

			terrain.setWorldOffset(mover->movement(terrain.worldOffset()));
			node.addNode(std::make_unique<Scenegraph::CustomNode<Renderer::Terrain>>(
				terrain,
				glm::translate(glm::mat4x4(1), glm::vec3(-50 * 200, 0, 0))
				));

			// Roads
			for (int i = 0; i < MAXROAD; i++) {
				road[i]->createMap(node, roadObject, streetLightObject, mover);
				road[i]->renderTree(node, tree);
			}

			// Ambient light
			node.addNode(std::make_unique<Scenegraph::LightNode<Renderer::AmbientLight>>(
				Renderer::AmbientLight(glm::vec3(0.02, 0.001, 0.1), renderContext), glm::mat4x4(1.f)
				));

			// Checks if a new note should be spawned
			if (mover->noteSpawn()) {
				/*notes.push_back(Note(renderContext, COLORS[rand() % COLORS->length()], (int)mover->randNr(5)));
				obstacles.push_back(Obstacle((int)mover->randNr(5), (int)mover->randNr(1)));*/

				//Check if equals to zero for when the game starts
				if (patternCount == MAX_PATTERNS || patternCount == 0) {
					patternCount = 0;

					// Was unable to get this into it's own file as I could not have a 
					// reference to an array
					std::ifstream innFil("resources/patterns/pattern" + std::to_string((int)mover->randNr(PATTERNCOUNT)) + ".txt");

					if (innFil) {
						for (int i = 0; i < MAX_PATTERNS; i++) {
							for (int j = 0; j < 5; j++) {
								innFil >> patterns[i][j];
							}
							innFil.ignore();
						}
					}
					else {
						std::cout << "Unable to find file: \n";
					}
				}

				patternControl(renderContext, mover, obstacles, notes, patternCount, patterns);

			}

			//Render the notes and get points if it is needed
			points += NoteControl(node, noteObject, mover, carRelativeLocation, notes);

			//Render the obstacles
			if (!ObstacleControl(node, testModel, mover, carRelativeLocation, obstacles)) {
				mover->resetValues();
				notes.clear();
				obstacles.clear();

				//Show high score (not implemented yet)

				// Updates highscore if it is needed
				updateHighScore(points);

				//Set back to start menu
				start_game = true;

				points = 0;
				//Start from the start of a new pattern
				patternCount = 0;
				carRelativeLocation = 2;
			}

			ImGui::Render();
			renderContext.render(windowWidth, windowHeight, node);

			mover->updateTime();
			mover->increaseSpeed(points);		//Updates the speed of the game

		}

		else ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		glfwSwapBuffers(window);
		glfwPollEvents();

	}

	glfwDestroyWindow(window);
	ImGui_ImplGlfw_Shutdown();
	ImGui_ImplOpenGL3_Shutdown();
	ImGui::DestroyContext();
	glfwTerminate();
	return 0;

	updateHighScore(points); // if the program shuits down and the points have not been reset and is higher then the previous record

	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR pCmdLine, int nCmdShow) {
	return main();
}
