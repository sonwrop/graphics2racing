# Graphics2Racing

## Proposal

The project will be a musical rhythm car game. The player must move their car
between several tracks to avoid obstacles, while also collecting 'notes' to
gain points.

 - **How it relates to the graphics course:** the project will be designed with
   a retro 'synthwave' feel, which is quite graphics heavy. This will involve
   a large number of lights, shadows, post-processing effects (such as bloom),
   models, and vector graphics.
 - **Which specific features we plan to implement:** for the first version of
   the game, we will have a single level with a prebuilt 'track' (i.e the
   positions of obstacles and notes along the road). We will also have a menu
   and a high-score system. On the graphics side, we will use a simple
   scenegraph system, a deferred renderer in order to support any number of
   lights, as well as a post-processing pipeline to get the look we want.
   For lighting, the renderer will use a PBR lighting model with support for
   shadows for directional lights using shadow maps. The renderer will support
   HDR, and will use gamma correction and FXAA. Models in the game, such as the
   player's car, obstacles, notes, and the road, will be loaded from OBJ files
   with Assimp.
 - **What we expect to learn from it:** the project will be valuable in learning
   the nuances of scenegraphs, model loading, and lighting. Ideally it will
   also provide an opportunity to learn about structuring a game engine, and
   separation of concerns when it comes to rendering and game logic.

## Setup
- Download the repository to a local folder on your system
- open CMakeLists.txt within the same folder as this README file with cmakle
- Select Graphics2Racing.exe in the dropdown menu for exes to run
- Run the exe and the program should work

## Patterns
The patterns are in a file that is 30x5 with the numbers 0, 1 and 2.
The files follow a name patter of pattern<number> where the numbers start at 0 and increase for each new pattern.
0 is for when the spot is empty.
1 is for a note.
2 is for an obstacle.

Example:
0 0 1 0 0
0 2 1 2 0
0 1 0 2 0
0 2 0 1 0

Only one pattern will be loaded at all times, when the pattern is gone through, a new one will randomly be selected.
Due to the naming convention, there is only the need to change one const for each pattern that is added.
The changed const is for the amount of available patterns in the game.

## How to play
You move the car using A to move left and D to more right.
Your objective is to collect the glowing notes while avoiding cars.

## Sources for resources
- Note: https://free3d.com/3d-model/musical-note-v2--897744.html
- Car: https://free3d.com/3d-model/lamborghini-aventador-42591.html
- Road: https://free3d.com/3d-model/roadsectionstraight-v1--66172.html (Texture edited by jonaseha)
- Street Lights: https://free3d.com/3d-model/street-light-lamp-61903.html
- Palm Tree: https://free3d.com/3d-model/date-palm-2286.html
- Music: https://youtu.be/Ak1-qLbHHCM