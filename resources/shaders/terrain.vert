layout(location=0) in vec2 rectPos;

out vec3 gsPos;

uniform ivec2 mapSize;
uniform vec2 tileSize;
uniform vec2 worldOffset;

uniform mat4 modelMatrix;

void main() {
    ivec2 mapPos = ivec2(gl_InstanceID % mapSize.x, gl_InstanceID / mapSize.x);
    vec2 tileTopLeft = mapPos * tileSize;
    vec2 tileBottomRight = tileTopLeft + tileSize;
    vec2 terrainPos = mix(tileTopLeft, tileBottomRight, rectPos);
    vec4 worldPos = modelMatrix * vec4(terrainPos.x, 0, terrainPos.y, 1);
    worldPos.y = heightMap(worldPos.xz + worldOffset) - 50;
    vec4 clipPos = camera.viewProjMatrix * worldPos;

    gsPos = worldPos.xyz;
    gl_Position = clipPos;
}
