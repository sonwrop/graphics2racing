layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in vec3 gsPos[3];

out vec3 fsPos;
out vec3 fsNormal;

void main() {
    vec3 u = gsPos[2] - gsPos[0];
    vec3 v = gsPos[1] - gsPos[0];
    vec3 n = -normalize(cross(u, v));

    for (int i = 0; i < 3; i++) {
        fsPos = gsPos[i];
        fsNormal = n;
        gl_Position = gl_in[i].gl_Position;
        EmitVertex();
    }
}
