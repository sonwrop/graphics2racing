layout(location=0) in vec2 linePos;
layout(location=1) in vec2 rectStartPos;
layout(location=2) in vec2 rectEndPos;

out vec3 fsPos;

uniform ivec2 mapSize;
uniform vec2 tileSize;
uniform float lineThickness;
uniform vec2 worldOffset;

uniform mat4 modelMatrix;

void main() {
    ivec2 mapPos = ivec2(gl_InstanceID % mapSize.x, gl_InstanceID / mapSize.x);
    vec2 tileTopLeft = mapPos * tileSize;
    vec2 tileBottomRight = tileTopLeft + tileSize;

    vec2 myRectPos = mix(rectStartPos, rectEndPos, linePos.y);
    vec2 myTerrainPos = mix(tileTopLeft, tileBottomRight, myRectPos);
    vec4 myWorldPos = modelMatrix * vec4(myTerrainPos.x, 0, myTerrainPos.y, 1);
    myWorldPos.y = heightMap(myWorldPos.xz + worldOffset);
    vec4 myClipPos = camera.viewProjMatrix * myWorldPos;

    vec2 connectedRectPos = mix(rectStartPos, rectEndPos, 1 - linePos.y);
    vec2 connectedTerrainPos = mix(tileTopLeft, tileBottomRight, connectedRectPos);
    vec4 connectedWorldPos = modelMatrix * vec4(connectedTerrainPos.x, 0, connectedTerrainPos.y, 1);
    connectedWorldPos.y = heightMap(connectedWorldPos.xz + worldOffset);
    vec4 connectedClipPos = camera.viewProjMatrix * connectedWorldPos;

    vec2 thicknessVector = normalize(myClipPos.xy / myClipPos.w - connectedClipPos.xy / connectedClipPos.w).yx * vec2(-1, 1);
    myClipPos.xy += thicknessVector * lineThickness * (1. - linePos.x * 2.) * myClipPos.w;

    fsPos = myWorldPos.xyz;
    gl_Position = myClipPos;
}
